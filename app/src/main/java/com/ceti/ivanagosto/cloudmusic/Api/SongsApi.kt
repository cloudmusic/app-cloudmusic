package com.ceti.ivanagosto.cloudmusic.Api

import android.util.JsonReader
import android.util.Log
import com.ceti.ivanagosto.cloudmusic.Models.AlbumModel
import com.ceti.ivanagosto.cloudmusic.Models.SongModel
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.URL

class SongsApi():ApiRest(){
    fun getSongs(album_id:String):ArrayList<SongModel>{
        var con:HttpURLConnection
        if(album_id!=""){
            con=this._newCon("songs/"+album_id,"GET")
        }
        else{
            con=this._newCon("songs","GET")
        }
        con.setRequestProperty("Authorization", this.token)

        try {
            if(con.responseCode==200){
                var response = InputStreamReader(con.inputStream)
                var data = JsonReader(response)
                data.beginObject()

                var songs = arrayListOf<SongModel>()
                while (data.hasNext()){
                    when(data.nextName()){
                        "songs" -> {
                            data.beginArray()
                            while (data.hasNext()){
                                var song = SongModel("false",0,"","0","", "")
                                data.beginObject()
                                while (data.hasNext()){
                                    when(data.nextName()){
                                        "_id" -> song._id=data.nextString()
                                        "number" -> song.number=data.nextInt()
                                        "name" -> song.name=data.nextString()
                                        "duration" -> song.duration=data.nextString()
                                        "file" -> song.file = data.nextString()
                                        else -> data.skipValue()
                                    }
                                }
                                data.endObject()
                                songs.add(song)
                            }
                            data.endArray()
                        }
                        else -> data.skipValue()
                    }
                }
                data.close()
                con.disconnect()
                return songs
            }else{
                Log.d("Codigo:",con.responseCode.toString())
                Log.d("Texto:",con.responseMessage)
                Log.d("metodo:",con.requestMethod)
            }
        }catch (err:Exception){
            Log.d("Error songs:",err.message)
        }
        con.disconnect()

        return arrayListOf<SongModel>()
    }

    fun getSong(song_id:String):SongModel{
        var con=this._newCon("song/"+song_id,"GET")
        con.setRequestProperty("Authorization", this.token)
        var song=SongModel("",0,"","0","", "")
        try {
            if (con.responseCode == 200) {
                var response = InputStreamReader(con.inputStream)
                var data = JsonReader(response)
                data.beginObject()
                while (data.hasNext()){
                    when(data.nextName()){
                        "song" -> {
                            data.beginObject()
                            while (data.hasNext()){
                                when(data.nextName()){
                                    "number" -> song.number = data.nextInt()
                                    "name" -> song.name = data.nextString()
                                    "duration" -> song.duration = data.nextString()
                                    "file" -> song.file = data.nextString()
                                    "album" -> {
                                        data.beginObject()
                                        while (data.hasNext()){
                                            when(data.nextName()){
                                                "image" -> song.image = data.nextString()
                                                else -> data.skipValue()
                                            }
                                        }
                                        data.endObject()
                                    }
                                    else -> data.skipValue()
                                }
                            }
                            data.endObject()
                        }
                        else -> data.skipValue()
                    }
                }
                data.close()
            }
        }catch (err:Exception){
            Log.d("Error:",err.message)
        }
        con.disconnect()
        return song
    }
}