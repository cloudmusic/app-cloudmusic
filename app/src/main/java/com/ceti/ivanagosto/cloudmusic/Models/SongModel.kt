package com.ceti.ivanagosto.cloudmusic.Models

import java.io.Serializable

class SongModel(var _id:String, var number:Int, var name:String, var duration:String, var file:String, var image:String):Serializable