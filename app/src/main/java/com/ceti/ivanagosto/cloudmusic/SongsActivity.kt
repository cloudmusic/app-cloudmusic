package com.ceti.ivanagosto.cloudmusic

import android.os.AsyncTask
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import com.ceti.ivanagosto.cloudmusic.Adapters.SongAdapter
import com.ceti.ivanagosto.cloudmusic.Api.SongsApi
import com.ceti.ivanagosto.cloudmusic.Models.SongModel

class SongsActivity : AppCompatActivity() {
    private lateinit var recyclerView: RecyclerView
    private lateinit var viewAdapter: RecyclerView.Adapter<*>
    private lateinit var viewManager: RecyclerView.LayoutManager

    private var api: SongsApi = SongsApi()
    private var database:Database = Database(this)
    private var album_id:String = ""

    public lateinit var songs:ArrayList<SongModel>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_songs)

        viewManager = LinearLayoutManager(this)
        viewAdapter = SongAdapter(arrayListOf())

        recyclerView = findViewById<RecyclerView>(R.id.songRecycler).apply {
            // use this setting to improve performance if you know that changes
            // in content do not change the layout size of the RecyclerView
            setHasFixedSize(true)

            // use a linear layout manager
            layoutManager = viewManager

            // specify an viewAdapter (see also next example)
            adapter = viewAdapter

        }
        try {
            this.album_id = this.intent.extras.getString("album")
        }catch (err:Exception){

        }
        this.getSongs()
    }

    fun getSongs(){
        var token = database.getToken()
        api.token=token
        AsyncTask.execute{
            var songs = api.getSongs(this.album_id)
            this.songs = songs
            this.makeRows()
        }
    }

    fun makeRows(){
        runOnUiThread {
            viewAdapter = SongAdapter(this.songs)

            recyclerView = findViewById<RecyclerView>(R.id.songRecycler).apply {
                // use this setting to improve performance if you know that changes
                // in content do not change the layout size of the RecyclerView
                setHasFixedSize(true)

                // use a linear layout manager
                layoutManager = viewManager

                // specify an viewAdapter (see also next example)
                adapter = viewAdapter

            }
        }
    }
}
