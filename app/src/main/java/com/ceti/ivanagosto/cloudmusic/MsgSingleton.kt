package com.ceti.ivanagosto.cloudmusic

import android.content.Context

object msgHelper {
    var _context: Context ?= null

    fun setContext(context: Context) {
        this._context = context
    }

    fun Toast(text: String?) {
        if(this._context == null) { return }
        android.widget.Toast.makeText(this._context, text, android.widget.Toast.LENGTH_SHORT).show()
    }

}