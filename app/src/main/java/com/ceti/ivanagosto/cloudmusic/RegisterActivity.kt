package com.ceti.ivanagosto.cloudmusic

import android.os.AsyncTask
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Looper
import com.ceti.ivanagosto.cloudmusic.Api.ApiRest
import com.ceti.ivanagosto.cloudmusic.Models.UserModel
import kotlinx.android.synthetic.main.activity_register.*

class RegisterActivity : AppCompatActivity() {
    private val api: ApiRest = ApiRest()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        msgHelper.setContext(this)
        registerBtn.setOnClickListener { register() }
    }

    fun register(){
        if(passwordText.text.toString().equals(repeatPasswordText.text.toString())){
            msgHelper.Toast("Registrado!")
            AsyncTask.execute {
                Looper.prepare()
                var user = UserModel(nameText.text.toString(), surnameText.text.toString(),
                        emailText.text.toString(), "", 0)
                var register = api.register(user,passwordText.text.toString())

                if(register){
                    msgHelper.Toast("Registrado!")
                    this.finishReg()
                }
            }
        }
        else{
            msgHelper.Toast("Contraseñas no coinciden.")
        }
    }

    fun finishReg(){
        this.finish()
    }
}
