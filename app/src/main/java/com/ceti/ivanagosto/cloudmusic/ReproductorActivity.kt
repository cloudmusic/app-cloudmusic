package com.ceti.ivanagosto.cloudmusic

import android.os.AsyncTask
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.ceti.ivanagosto.cloudmusic.Api.SongsApi
import com.ceti.ivanagosto.cloudmusic.Models.SongModel
import kotlinx.android.synthetic.main.activity_reproductor.*
import android.graphics.drawable.Drawable
import android.media.MediaPlayer
import android.os.Looper
import android.util.Log
import java.io.InputStream
import java.net.URL
import java.text.FieldPosition
import java.util.*
import kotlin.collections.ArrayList
import kotlin.concurrent.schedule


class ReproductorActivity : AppCompatActivity() {
    private var api: SongsApi = SongsApi()
    private var database:Database = Database(this)
    lateinit var song_id:String
    var song:SongModel = SongModel("",0, "", "0", "", "")
    lateinit var songList:ArrayList<SongModel>
    var position: Int = 0
    var player:MediaPlayer = MediaPlayer()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reproductor)
        msgHelper.setContext(this)

        try {
            this.song_id = this.intent.extras.getString("song")
            this.songList =  this.intent.extras.getSerializable("songList") as ArrayList<SongModel>
            this.position = this.intent.extras.getInt("position")
            this.getSong()
            playBtn.setOnClickListener { this.actionPlay() }
            nextBtn.setOnClickListener { this.nextSong() }
            previousBtn.setOnClickListener { this.prevSong() }
        }catch (err:Exception){
            this.finish()
            Log.d("Err:",err.message)
            msgHelper.Toast("Ocurrio un error.")
        }
    }

    fun getSong(){
        var token = database.getToken()
        api.token=token

        AsyncTask.execute{
            Looper.prepare()
            var song = api.getSong(this.song_id)
            this.song = song
            this.setView()
        }
    }

    fun setView(){
        nameTxt.text = this.song.name

        val image = URL(api._url+"get-image-album/"+song.image).getContent() as InputStream
        val img = Drawable.createFromStream(image, "song")
        player.setDataSource(api._url+"get-file-song/"+song.file)
        player.prepare()
        seekBar.max = player.duration
        player.start()
        this.seekMover()
        runOnUiThread {
            songImg.setImageDrawable(img)
        }
    }

    fun actionPlay(){
        if(this.player.isPlaying){
            this.player.pause()
            playBtn.setImageResource(android.R.drawable.ic_media_play)
        }
        else{
            this.player.start()
            playBtn.setImageResource(android.R.drawable.ic_media_pause)
        }
    }

    fun seekMover(){
        val pos = player.currentPosition
        seekBar.progress = pos
        Timer("Prueba").schedule(1000){
            seekMover()
        }
    }

    fun nextSong(){
        player.stop()
        player.reset()
        if(position+1<songList.size){
            position++
        }else{
            position = 0
        }
        this.song_id = this.songList[position]._id
        this.getSong()
    }

    fun prevSong(){
        player.stop()
        player.reset()
        if(position-1>=0){
            position--
        }else{
            position = this.songList.size
        }
        this.song_id = this.songList[position]._id
        this.getSong()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        player.stop()
        this.finishActivity(0)
    }

}
