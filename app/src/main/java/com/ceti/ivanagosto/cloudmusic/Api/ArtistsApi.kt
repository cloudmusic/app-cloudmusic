package com.ceti.ivanagosto.cloudmusic.Api

import android.util.JsonReader
import android.util.Log
import com.ceti.ivanagosto.cloudmusic.Models.ArtistModel
import java.io.InputStreamReader

class ArtistsApi(): ApiRest() {

    fun getArtists():ArrayList<ArtistModel>{
        var con=this._newCon("artists","GET")
        con.setRequestProperty("Authorization", this.token)

        try {
            if(con.responseCode==200){
                var response = InputStreamReader(con.inputStream)
                var data = JsonReader(response)
                data.beginObject()

                var artists = arrayListOf<ArtistModel>()
                while (data.hasNext()){
                    when(data.nextName()){
                        "artists" -> {
                            data.beginArray()
                            while (data.hasNext()){
                                var artist = ArtistModel("false","","")
                                data.beginObject()
                                while (data.hasNext()){
                                    when(data.nextName()){
                                        "_id" -> artist._id=data.nextString()
                                        "name" -> artist.name=data.nextString()
                                        "description" -> artist.description=data.nextString()
                                        else -> data.skipValue()
                                    }
                                }
                                data.endObject()
                                artists.add(artist)
                            }
                            data.endArray()
                        }
                        else -> data.skipValue()
                    }
                }
                data.close()
                con.disconnect()
                return artists
            }else{
                Log.d("Codigo:",con.responseCode.toString())
                Log.d("Texto:",con.responseMessage)
                Log.d("metodo:",con.requestMethod)
            }
        }catch (err:Exception){
            Log.d("Error artistas:",err.message)
        }
        con.disconnect()

        return arrayListOf<ArtistModel>()
    }
}