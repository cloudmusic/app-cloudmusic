package com.ceti.ivanagosto.cloudmusic

import android.content.Intent
import android.os.AsyncTask
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Looper
import android.widget.Toast
import com.ceti.ivanagosto.cloudmusic.Api.ApiRest
import com.ceti.ivanagosto.cloudmusic.Models.UserModel
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {
    private val api: ApiRest = ApiRest()
    private val database:Database = Database(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        msgHelper.setContext(this)
        if(database.getUser().status==200 && !database.getToken().equals("false")){
            startActivity(Intent(this,MainActivity::class.java))
        }
        registerBtn.setOnClickListener { launchRegister() }
        loginBtn.setOnClickListener { launchMain() }
    }

    fun launchRegister(){
        startActivity(Intent(this,RegisterActivity::class.java))
    }

    fun launchMain(){
        loginBtn.isEnabled=false
        AsyncTask.execute {
            val user: UserModel = api.login(emailText.text.toString(),passwordText.text.toString())
            Looper.prepare()
            if(user.status==200){
                database.newUser(user)
                this.getToken(emailText.text.toString(),passwordText.text.toString())
                msgHelper.Toast("Bienvenido "+user.name+".")
            }
            else{
                Toast.makeText(this,"Datos invalidos.",Toast.LENGTH_SHORT).show()
                loginBtn.isEnabled=true
            }
        }

    }

    private fun getToken(email:String,password:String){
        AsyncTask.execute {
            val token=api.getToken(email,password)
            database.newToken(token)
            startActivity(Intent(this, MainActivity::class.java))
        }
    }
}
