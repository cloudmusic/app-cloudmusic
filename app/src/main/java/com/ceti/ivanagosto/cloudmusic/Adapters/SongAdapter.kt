package com.ceti.ivanagosto.cloudmusic.Adapters

import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import com.ceti.ivanagosto.cloudmusic.Models.SongModel
import com.ceti.ivanagosto.cloudmusic.R
import com.ceti.ivanagosto.cloudmusic.ReproductorActivity
import java.io.Serializable

class SongAdapter(private val myDataset: ArrayList<SongModel>) :
        RecyclerView.Adapter<SongAdapter.ViewHolder>() {

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder.
    // Each data item is just a string in this case that is shown in a TextView.
    class ViewHolder(val view: View) : RecyclerView.ViewHolder(view){
        val songText : TextView
        val songRow : LinearLayout

        init {
            // Define click listener for the ViewHolder's View
            songText = view.findViewById(R.id.songText)
            songRow = view.findViewById(R.id.songsLay)
        }
    }


    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): ViewHolder {
        // create a new view
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.view_songs, parent, false)
        // set the view's size, margins, paddings and layout parameters

        return ViewHolder(view)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        holder.songText.text = myDataset[position].name

        holder.songRow.setOnClickListener {
            Toast.makeText(it.context, holder.songText.text.toString(), Toast.LENGTH_SHORT).show()
            val intent = Intent(it.context, ReproductorActivity::class.java)
            intent.putExtra("song", myDataset[position]._id)
            // val m = Bundle()
            // m.putSerializable("songList",myDataset)
            // intent.putExtras(m)
            intent.putExtra("songList", myDataset)
            intent.putExtra("position", position)
            it.context.startActivity(intent)
        }
    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount() = myDataset.size
}