package com.ceti.ivanagosto.cloudmusic.Api

import android.util.JsonReader
import android.util.Log
import com.ceti.ivanagosto.cloudmusic.Models.UserModel
import java.io.FileNotFoundException
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.URL
import java.util.concurrent.TimeoutException

open class ApiRest(){
    // Local
    // private val _url="http://192.168.0.25:3799/api/"

    // Produccion
    public val _url="http://80.211.44.142:3799/api/"

    public var token:String=""


    protected fun _newCon(uri:String,method:String):HttpURLConnection{
        var url = URL(this._url+uri)
        var con = url.openConnection() as HttpURLConnection

        con.setRequestProperty("User-Agent", "insomnia/5.16.6")
        con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded")
        con.setRequestProperty("Accept", "*/*")
        con.requestMethod=method
        con.connectTimeout=10000
        con.readTimeout=10000

        if(method.equals("POST"))
            con.doOutput=true

        return con
    }

    fun login(user:String, password:String): UserModel {
        var con=this._newCon("login","POST")
        val params:String="email="+user+"&password="+password
        con.outputStream.write(params.toByteArray())



        try {
            if(con.responseCode==200){

                var user = UserModel("", "", "", "", 200)

                var response = InputStreamReader(con.inputStream)
                var data = JsonReader(response)
                data.beginObject()

                while (data.hasNext()){
                    val key = data.nextName()
                    if(key.toString().equals("user")){
                        data.beginObject()
                        while (data.hasNext()){
                            val k=data.nextName()
                            when(k.toString()){
                                "name" -> {
                                    user.name=data.nextString()
                                }
                                "surname" -> {
                                    user.surname=data.nextString()
                                }
                                "email" -> {
                                    user.email=data.nextString()
                                }
                                "role" -> {
                                    user.role=data.nextString()
                                }
                                else -> {
                                    data.skipValue()
                                }
                            }

                        }
                        data.endObject()
                    }
                    else{
                        data.skipValue()
                    }
                }
                data.close()
                con.disconnect()
                return user
            }
            else{
                Log.d("ResCode",con.responseCode.toString())
                Log.d("Texto:",con.responseMessage)
            }

        }catch (err:FileNotFoundException){
            Log.d("Error",err.message)
            Log.d("ResCode",con.responseCode.toString())
            Log.d("Texto:",con.responseMessage)
        }catch (err:TimeoutException){
            Log.d("Tiempo vencido:",err.localizedMessage)
            Log.d("Error",err.message)
        }catch (err:Exception){
            Log.d("Error",err.message)
        }
        con.disconnect()
        var user= UserModel("", "", "", "", 0)
        return user
    }

    fun getToken(user:String, password:String):String{
        var con=this._newCon("login","POST")
        val params:String="email="+user+"&password="+password+"&getHash=true"
        con.outputStream.write(params.toByteArray())

        try {
            if (con.responseCode == 200) {
                var response = InputStreamReader(con.inputStream)
                var data = JsonReader(response)
                data.beginObject()

                if(data.hasNext()){
                    if(data.nextName().toString().equals("token")){
                        return data.nextString().toString()
                    }
                }

            }
        }catch (err:FileNotFoundException){
            Log.d("Error",err.message)
            Log.d("ResCode",con.responseCode.toString())
            Log.d("Texto:",con.responseMessage)
        }catch (err:TimeoutException){
            Log.d("Tiempo vencido:",err.localizedMessage)
            Log.d("Error",err.message)
        }

        return "false"
    }

    fun register(user: UserModel, password: String):Boolean{
        var con=this._newCon("register","POST")
        val params:String="name="+user.name+
                "&surname="+user.surname+
                "&email="+user.email+
                "&password="+password
        con.outputStream.write(params.toByteArray())
        try {
            if(con.responseCode==200){
                return true
            }
        }catch (err:FileNotFoundException){
            Log.d("Error al registrar:",err.message)
        }catch (err:TimeoutException){
            Log.d("Tiempo vencido:",err.localizedMessage)
            Log.d("Error",err.message)
        }
        return false
    }
}