package com.ceti.ivanagosto.cloudmusic.Adapters

import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import com.ceti.ivanagosto.cloudmusic.Models.AlbumModel
import com.ceti.ivanagosto.cloudmusic.R
import com.ceti.ivanagosto.cloudmusic.SongsActivity

class AlbumAdapter(private val myDataset: ArrayList<AlbumModel>) :
        RecyclerView.Adapter<AlbumAdapter.ViewHolder>() {

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder.
    // Each data item is just a string in this case that is shown in a TextView.
    class ViewHolder(val view: View) : RecyclerView.ViewHolder(view){
        val albumText: TextView
        val albumRow: LinearLayout

        init {
            // Define click listener for the ViewHolder's View
            albumText = view.findViewById(R.id.albumText)
            albumRow = view.findViewById(R.id.albumsLay)
        }
    }


    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): ViewHolder {
        // create a new view
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.view_albums, parent, false)
        // set the view's size, margins, paddings and layout parameters

        return ViewHolder(view)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        holder.albumText.text = myDataset[position].title
        holder.albumRow.setOnClickListener {
            Toast.makeText(it.context,holder.albumText.text.toString(), Toast.LENGTH_SHORT).show()
            val intent = Intent(it.context, SongsActivity::class.java)
            intent.putExtra("album",myDataset[position]._id)
            it.context.startActivity(intent)
        }
    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount() = myDataset.size
}