package com.ceti.ivanagosto.cloudmusic

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.ceti.ivanagosto.cloudmusic.Api.ApiRest
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private val api: ApiRest = ApiRest()
    private val database:Database = Database(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        artistList.setOnClickListener { launchArtists() }
        albumList.setOnClickListener { launchAlbums() }
        songList.setOnClickListener { launchSongs() }

        logoutBtn.setOnClickListener { logout() }
    }

    fun logout(){
        database.logout()
        startActivity(Intent(this,LoginActivity::class.java))
    }

    fun launchArtists(){
        startActivity(Intent(this,ArtistsActivity::class.java))
    }

    fun launchAlbums(){
        startActivity(Intent(this,AlbumsActivity::class.java))
    }

    fun launchSongs(){
        startActivity(Intent(this,SongsActivity::class.java))
    }

}
