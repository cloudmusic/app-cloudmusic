package com.ceti.ivanagosto.cloudmusic

import android.os.AsyncTask
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import com.ceti.ivanagosto.cloudmusic.Adapters.ArtistAdapter
import com.ceti.ivanagosto.cloudmusic.Api.ArtistsApi
import com.ceti.ivanagosto.cloudmusic.Models.ArtistModel

class ArtistsActivity : AppCompatActivity() {
    private lateinit var recyclerView: RecyclerView
    private lateinit var viewAdapter: RecyclerView.Adapter<*>
    private lateinit var viewManager: RecyclerView.LayoutManager
    private var api:ArtistsApi = ArtistsApi()
    private var database:Database = Database(this)

    public lateinit var artists:ArrayList<ArtistModel>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_artists)
        viewManager = LinearLayoutManager(this)
        viewAdapter = ArtistAdapter(arrayListOf(), this)

        recyclerView = findViewById<RecyclerView>(R.id.artistRecycler).apply {
            // use this setting to improve performance if you know that changes
            // in content do not change the layout size of the RecyclerView
            setHasFixedSize(true)

            // use a linear layout manager
            layoutManager = viewManager

            // specify an viewAdapter (see also next example)
            adapter = viewAdapter

        }
        this.getArtist()
        msgHelper.setContext(this)
    }

    fun getArtist(){
        var token = database.getToken()
        api.token=token
        AsyncTask.execute{
            var artists = api.getArtists()
            this.artists=artists
            this.makeRows()
        }
    }

    fun makeRows(){
        runOnUiThread {
            viewAdapter = ArtistAdapter(this.artists, this)

            recyclerView = findViewById<RecyclerView>(R.id.artistRecycler).apply {
                // use this setting to improve performance if you know that changes
                // in content do not change the layout size of the RecyclerView
                setHasFixedSize(true)

                // use a linear layout manager
                layoutManager = viewManager

                // specify an viewAdapter (see also next example)
                adapter = viewAdapter

            }
        }
    }
}
