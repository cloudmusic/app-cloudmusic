package com.ceti.ivanagosto.cloudmusic

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteException
import android.database.sqlite.SQLiteOpenHelper
import com.ceti.ivanagosto.cloudmusic.Models.UserModel

class Database(context:Context): SQLiteOpenHelper(context,"pingPong",null,1) {
    val dbName="pingPong"

    private val dbUsers="CREATE TABLE users(id INTEGER PRIMARY KEY AUTOINCREMENT, name varchar(30), " +
            "surname varchar(30), email string, role varchar(30), status integer)"
    private val dbTokens="CREATE TABLE tokens(id INTEGER PRIMARY KEY AUTOINCREMENT, token string,status int)"
    override fun onCreate(db: SQLiteDatabase?) {
        db?.execSQL(dbUsers)
        db?.execSQL(dbTokens)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db?.execSQL("DROP TABLE users")
        db?.execSQL("DROP TABLE tokens")
        onCreate(db)
    }

    private fun closeUsers(){
        val db= writableDatabase
        db.execSQL("UPDATE users SET status=404 WHERE 1")
    }

    private fun closeTokens(){
        val db= writableDatabase
        db.execSQL("UPDATE tokens SET status=404 WHERE 1")
    }

    fun newUser(data: UserModel): Boolean {
        val db= writableDatabase
        this.closeUsers()
        val user=ContentValues()
        user.put("name",data.name.toString())
        user.put("surname",data.surname.toString())
        user.put("email",data.email.toString())
        user.put("role",data.role.toString())
        user.put("status",data.status.toInt())

        db.insert("users",null,user)

        return true
    }

    fun getUser(): UserModel {
        val db = writableDatabase
        var user= UserModel("", "", "", "", 500)

        try {
            user= UserModel("", "", "", "", 200)
            var cursor= db.rawQuery("SELECT * FROM users WHERE status=200 ORDER BY id DESC LIMIT 1",null)
            cursor.moveToFirst()

            user.name=cursor.getString(cursor.getColumnIndex("name")).toString()
            user.surname=cursor.getString(cursor.getColumnIndex("surname")).toString()
            user.email=cursor.getString(cursor.getColumnIndex("email")).toString()
            user.role=cursor.getString(cursor.getColumnIndex("role")).toString()

            cursor.close()

            return user

        }catch (e:SQLiteException){
            db?.execSQL(dbUsers)
        }catch (e:Exception){
            user= UserModel("", "", "", "", 500)
        }
        return user
    }

    fun newToken(token:String): Boolean {
        val db= writableDatabase
        this.closeTokens()
        val newToken=ContentValues()
        newToken.put("token",token.toString())
        newToken.put("status",1)

        db.insert("tokens",null,newToken)

        return true
    }

    fun getToken(): String{
        val db = writableDatabase
        var token=""

        try {
            var cursor= db.rawQuery("SELECT * FROM tokens WHERE status=1 ORDER BY id DESC LIMIT 1",null)
            cursor.moveToFirst()

            token = cursor.getString(cursor.getColumnIndex("token")).toString()

            cursor.close()

            return token

        }catch (e:SQLiteException){
            db?.execSQL(dbTokens)
        }catch (e:Exception){
            token=""
        }
        return token
    }

    fun logout(){
        this.closeUsers()
        this.closeTokens()
    }

}