package com.ceti.ivanagosto.cloudmusic

import android.content.Intent
import android.os.AsyncTask
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import com.ceti.ivanagosto.cloudmusic.Adapters.AlbumAdapter
import com.ceti.ivanagosto.cloudmusic.Api.AlbumsApi
import com.ceti.ivanagosto.cloudmusic.Models.AlbumModel

class AlbumsActivity : AppCompatActivity() {
    private lateinit var recyclerView: RecyclerView
    private lateinit var viewAdapter: RecyclerView.Adapter<*>
    private lateinit var viewManager: RecyclerView.LayoutManager

    private var api: AlbumsApi = AlbumsApi()
    private var database:Database = Database(this)
    private var artist_id = ""

    public lateinit var albums:ArrayList<AlbumModel>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_albums)

        viewManager = LinearLayoutManager(this)
        viewAdapter = AlbumAdapter(arrayListOf())

        recyclerView = findViewById<RecyclerView>(R.id.albumRecycler).apply {
            // use this setting to improve performance if you know that changes
            // in content do not change the layout size of the RecyclerView
            setHasFixedSize(true)

            // use a linear layout manager
            layoutManager = viewManager

            // specify an viewAdapter (see also next example)
            adapter = viewAdapter

        }
        try {
            this.artist_id = this.intent.extras.getString("artist")
        }catch (err:Exception){

        }

        msgHelper.setContext(this)
        // msgHelper.Toast("Artista: "+this.artist_id)
        this.getAlbums()
    }
    fun getAlbums(){
        var token = database.getToken()
        api.token=token
        AsyncTask.execute{
            var albums = api.getAlbums(this.artist_id)
            this.albums = albums
            this.makeRows()
        }
    }

    fun makeRows(){
        runOnUiThread {
            viewAdapter = AlbumAdapter(this.albums)

            recyclerView = findViewById<RecyclerView>(R.id.albumRecycler).apply {
                // use this setting to improve performance if you know that changes
                // in content do not change the layout size of the RecyclerView
                setHasFixedSize(true)

                // use a linear layout manager
                layoutManager = viewManager

                // specify an viewAdapter (see also next example)
                adapter = viewAdapter

            }
        }
    }
}
