package com.ceti.ivanagosto.cloudmusic.Api

import android.util.JsonReader
import android.util.Log
import com.ceti.ivanagosto.cloudmusic.Models.AlbumModel
import com.ceti.ivanagosto.cloudmusic.Models.ArtistModel
import java.io.InputStreamReader
import java.net.HttpURLConnection

class AlbumsApi():ApiRest(){
    fun getAlbums(artist_id:String):ArrayList<AlbumModel>{
        var con:HttpURLConnection
        if(artist_id!=""){
            con=this._newCon("albums/"+artist_id,"GET")
        }else{
            con=this._newCon("albums","GET")
        }
        con.setRequestProperty("Authorization", this.token)

        try {
            if(con.responseCode==200){
                var response = InputStreamReader(con.inputStream)
                var data = JsonReader(response)
                data.beginObject()

                var albums = arrayListOf<AlbumModel>()
                while (data.hasNext()){
                    when(data.nextName()){
                        "albums" -> {
                            data.beginArray()
                            while (data.hasNext()){
                                var album = AlbumModel("false","","",0)
                                data.beginObject()
                                while (data.hasNext()){
                                    when(data.nextName()){
                                        "_id" -> album._id=data.nextString()
                                        "title" -> album.title=data.nextString()
                                        "description" -> album.description=data.nextString()
                                        "year" -> album.year=data.nextInt()
                                        else -> data.skipValue()
                                    }
                                }
                                data.endObject()
                                albums.add(album)
                            }
                            data.endArray()
                        }
                        else -> data.skipValue()
                    }
                }
                data.close()
                con.disconnect()
                return albums
            }else{
                Log.d("Codigo:",con.responseCode.toString())
                Log.d("Texto:",con.responseMessage)
                Log.d("metodo:",con.requestMethod)
            }
        }catch (err:Exception){
            Log.d("Error albums:",err.message)
        }
        con.disconnect()

        return arrayListOf<AlbumModel>()
    }
}